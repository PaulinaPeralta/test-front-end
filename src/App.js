import React, { Component } from 'react';
import './App.css';
import SearchBox from './components/searchBox/searchBox.js';
import SearchResultsList from './components/searchResultsList/searchResultsList.js';
import ProductDetail from './components/productDetail/productDetail.js';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      search: '',
      itemId: '',
      categories: ''
    }
  };

  handleSearch = (search) => {
    localStorage.setItem("search", JSON.stringify(search));
  }

  getCategories = (categories) => {
    localStorage.setItem("categories", JSON.stringify(categories));
  }

  getItemId = (id) => {
    localStorage.setItem("itemId", JSON.stringify(id));
  }

  componentWillMount() {
    let id = "";
    let categories = "";
    let search = "";

    if (localStorage && localStorage.getItem("search")) {
      search = JSON.parse(localStorage.getItem("search"));
    }
    this.setState({search: search});

    if (localStorage && localStorage.getItem("itemId")) {
      id = JSON.parse(localStorage.getItem("itemId"));
    }
    this.setState({ itemId: id });

    if (localStorage && localStorage.getItem("categories")) {
      categories = JSON.parse(localStorage.getItem("categories"));
    }
    this.setState({categories: categories});
  }

  formatNumber = (price) => {
    return new Intl.NumberFormat('de-DE').format(price);
  }

  render() {
    return (
      <div className="App">
        <Router>
          <Switch>
            <Route path="/items/:id">
              <SearchBox onChangeSearch={this.handleSearch} />
              <ProductDetail id={this.state.itemId} categories={this.state.categories} formatNumber={this.formatNumber} />
            </Route>
            <Route path="/items">
              <SearchBox onChangeSearch={this.handleSearch} />
              <SearchResultsList search={this.state.search.search} getItemId={this.getItemId} getCategories={this.getCategories} formatNumber={this.formatNumber} />
            </Route>
            <Route exact path="/">
              <SearchBox onChangeSearch={this.handleSearch} />
            </Route>
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;
