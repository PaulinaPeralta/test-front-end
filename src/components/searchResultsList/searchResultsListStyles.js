const styles = theme => ({
    tableContainer: {
        width: '85%',
        display: 'inline-block',
        marginBottom: '84px',
        boxShadow: 'none',
    },
    image: {
        width: '180px',
        heigth: '180px',
        borderRadius: '4px',
    },
    price: {
        color: '#333333',
        fontSize: '24px',
    },
    description: {
        color: '#666666',
        fontSize: '18px',
    },
    location: {
        color: '#999999',
        fontSize: '14px',
    },
    div: {
        marginBottom: '32px',
    },
    tableRow: {
        heigth: '196px',
    },
    tableCellImg: {
        textAlign: 'center',
        width: '196px',
        padding: '16px',
    },
    tableCell: {
        padding: '0px',
    },
    link: {
        textDecoration: 'auto',
    },
    spinner: {
        marginTop: '15px',
    }
});

export default styles;