var express = require('express');
var request = require('superagent');

const app = express()

app.set("PORT", process.env.PORT || 5000)

app.get("/api", (req, res) => {
    res.send("Welcome to our api")
})


app.get("/api/items/:id", async (req, res) => {
    var id = req.params.id;
    var description = '';
    var detail = '';
    
    await request.get(`https://api.mercadolibre.com/items/${id}/description`)
        .then((res) => {
            description = JSON.parse(res.text).plain_text;
        });
    
    await request.get(`https://api.mercadolibre.com/items/${id}`)
        .then((res) => {
            const productDetail = JSON.parse(res.text);
            detail = {
                author: {
                    name: productDetail.name ? productDetail.name : '',
                    lastname: productDetail.lastname ? productDetail.lastname : '',
                },
                item: {
                    id: productDetail.id,
                    title: productDetail.title,
                    price: productDetail.price,
                    picture: productDetail.thumbnail,
                    condition: productDetail.condition,
                    free_shipping: productDetail.shipping.free_shipping,
                    sold_quantity: productDetail.sold_quantity,
                    description: description,
                }
            };
        });
    res.json(detail);
})

app.get("/api/items", async (req, res) => {
    var query = req.query.q;
    var items = '';
    var results = [];
    await request.get('https://api.mercadolibre.com/sites/MLA/search')
            .query({ q: query })
            .then(async (res) => {
                const searchResults = JSON.parse(res.text);
                searchResults.results.map((item, i) => {
                    if(i<4){
                        results.push({
                            id: item.id,
                            title: item.title,
                            price: item.price,
                            picture: item.thumbnail,
                            condition: item.condition,
                            free_shipping: item.shipping.free_shipping,
                            state_name: item.address.state_name,
                        });
                    }
                });
                if (searchResults.results.length > 0) {
                    await request.get(`https://api.mercadolibre.com/categories/${searchResults.results[0].category_id}`)
                        .then(async (res) => {
                            items = {
                                author: {
                                    name: searchResults.name ? searchResults.name : '',
                                    lastname: searchResults.lastname ? searchResults.lastname : '',
                                },
                                categories: await res.body.path_from_root,
                                items: results,
                            }
                        });
                }
            });
    res.json(items);
})

app.listen(app.get("PORT"), () => {
    console.log(`API running at: localhost:${app.get("PORT")}`)
})