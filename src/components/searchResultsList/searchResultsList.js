import React, { Component } from 'react';
import { Paper, TableContainer, Table, TableBody, TableRow, TableCell, CircularProgress } from '@material-ui/core';
import styles from './searchResultsListStyles';
import { withStyles } from '@material-ui/core/styles';
import Breadcrumb from '../breadcrumb/breadcrumb.js';

class SearchResultsList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            items: {},
            itemId: ''
        }
    }

    async componentDidMount() {
        const response = await fetch(`/api/items?q=${this.props.search}`);
        this.setState({
            items: await response.json()               
        });
        this.getCategories();
    }

    getId = (id) => {
        this.setState({itemId: id});
        this.props.getItemId(id);
    }

    getCategories = () => {
        this.props.getCategories(this.state.items.categories);
    }

      render() {
          const { classes, formatNumber } = this.props;
          const { items } = this.state.items;
          return (
            <div>
                  { items
                      ? <div>
                          <Breadcrumb categories={this.state.items.categories}></Breadcrumb>
                          <TableContainer className={classes.tableContainer} component={Paper}>
                              {items.map((item) => (
                                  <a href={`/items/${item.id}`} key={item.id} className={classes.link} onClick={() => this.getId(item.id)}>
                                      <div>
                                          <Table aria-label="custom pagination table">
                                              <TableBody>
                                                  <TableRow className={classes.tableRow}>
                                                      <TableCell className={classes.tableCellImg} width='25%'>
                                                          <img className={classes.image} src={item.picture}></img>
                                                      </TableCell>
                                                      <TableCell className={classes.tableCell} width='60%'>
                                                          <div>
                                                              <div className={classes.div}>
                                                                  <span className={classes.price}>${formatNumber(item.price)}</span>
                                                              </div>
                                                              <div>
                                                                  <span className={classes.description}>{item.title}</span>
                                                              </div>
                                                          </div>
                                                      </TableCell>
                                                      <TableCell className={classes.tableCell} width='15%'>
                                                          <span className={classes.location}>{item.state_name}</span>
                                                      </TableCell>
                                                  </TableRow>
                                              </TableBody>
                                          </Table>
                                      </div>
                                  </a>
                              ))}
                          </TableContainer>
                      </div>
                      : <CircularProgress className={classes.spinner}/>
                  }
              </div>
          );
    }
}

export default withStyles(styles)(SearchResultsList);