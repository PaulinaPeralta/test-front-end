import React, { Component } from 'react';
import styles from './breadcrumbStyles';
import { withStyles } from '@material-ui/core/styles';
import { Breadcrumbs, Typography} from '@material-ui/core';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';

class Breadcrumb extends Component {
      render() {
          const { classes, categories } = this.props;
          return (
              <Breadcrumbs separator={<NavigateNextIcon fontSize="small" />} aria-label="breadcrumb" className={classes.breadcrumbs}>
                { categories && categories.map((category, i) => (
                    <div key={i}>
                        {(i < categories.length - 1) && <Typography className={classes.typography1}>{category.name}</Typography>}
                        {(i === categories.length - 1) && <Typography className={classes.typography2}>{category.name}</Typography>}
                    </div>
                ))
                }
              </Breadcrumbs>
          );
    }
}

export default withStyles(styles)(Breadcrumb);