import React, { Component } from 'react';
import { Paper, IconButton, InputBase, Divider, AppBar, Toolbar } from '@material-ui/core';
import SearchIcon  from '@material-ui/icons/Search';
import styles from './searchBoxStyles';
import { withStyles } from '@material-ui/core/styles';

class SearchBox extends Component {
    constructor(props) {
        super(props);
        this.state = {
            search: ""
        };
    };
    
    handleChange = (e) => {
        this.setState({ search: e.target.value });
        this.props.onChangeSearch({ search: e.target.value });
    }
    
    render() {
        const { classes } = this.props;
        return (
            <AppBar position="static" className={classes.appBar}>
                <Toolbar className={classes.toolbar}>
                    <img className={classes.logo} src='https://logodownload.org/wp-content/uploads/2018/10/mercado-libre-logo-6.png'></img>
                    <Paper component="form" className={classes.root}>
                        <InputBase
                            className={classes.input}
                            placeholder="Nunca dejes de buscar"
                            inputProps={{ 'aria-label': 'Nunca dejes de buscar' }}
                            value={this.state.search}
                            onChange={this.handleChange}
                            onKeyPress={(e) => {
                                if (e.charCode === 13) {
                                    e.preventDefault();
                                 }
                            }}
                        />
                        <Divider orientation="vertical" className={classes.divider} />
                            <IconButton type="submit" aria-label="search" className={classes.iconButton} onClick={() => this.handleChange} href={`/items?search=${this.state.search}`}>
                                <SearchIcon />
                            </IconButton>
                    </Paper>
                </Toolbar>
            </AppBar>
        );
    }
}

export default withStyles(styles)(SearchBox);