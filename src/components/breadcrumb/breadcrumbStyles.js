const styles = theme => ({
    breadcrumbs: {
        marginTop: '16px',
        marginBottom: '16px',
        width: '85%',
        display: 'inline-block',
        fontSize: '14px',
        color: '#999999',
    },
    typography1: {
        color: '#999999',
        fontSize: '14px',
    },
    typography2: {
        color: '#999999',
        fontWeight: '600',
        fontSize: '14px',
    }
});

export default styles;