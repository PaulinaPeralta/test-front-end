const styles = theme => ({
    root: {
      padding: '2px 4px',
      display: 'flex',
      alignItems: 'center',
      width: '75%',
      height: '50%',
    },
    input: {
      marginLeft: theme.spacing(1),
      flex: 1,
      fontSize: '14px',
    },
    iconButton: {
      padding: 10,
    },
    divider: {
      height: 28,
      margin: 4,
    },
    appBar: {
      background: '#FFE600',
      height: '50px',
      justifyContent: 'center',
    },
    logo: {
      marginRight: '10px',
      height: '40px',
    },
    toolbar: {
      justifyContent: 'center',
    },
});

export default styles;