const styles = theme => ({
    root: {
        width: '85%',
        display: 'inline-block',
        marginBottom: '84px',
        boxShadow: 'none',
    },
    breadcrumbs: {
        marginTop: '16px',
        marginBottom: '16px',
        width: '85%',
        display: 'inline-block',
        fontSize: '14px',
        color: '#999999',
    },
    button: {
        borderRadius: '6px',
        background: '#3483FA',
        borderColor: 'transparent',
        height: '48px',
        width: '65%',
        marginTop: '32px',
        marginRigth: '32px',
    },
    span: {
        color: '#ffffff',
        fontSize: '16px',
        fontWeight: '550',
        textTransform: 'none',
        font: '400 13.3333px Arial',
        fontFamily: 'Proxima Nova,-apple-system,Helvetica Neue,Helvetica,Roboto,Arial,sans-serif',
    },
    cardActions: {
        justifyContent: 'flex-end',
    },
    image: {
        maxWidth: '680px',
        maxHeigth: '680px',
        borderRadius: '4px',
        height: '480px',
    },
    div: {
        textAlign: 'justify',
        padding: '50px',
    },
    divSpan1: {
        marginTop: '32px',
        marginBottom: '16px',
    },
    span1: {
        color: '#999999',
        fontSize: '14px',
        fontWeight: '400',
        verticalAlign: '3px',
        whiteSpace: 'pre-wrap',
    },
    divSpan2:{
        marginBottom: '32px',
    },
    span2: {
        color: '#333333',
        fontSize: '24px',
        fontWeight: '600',
        paddingBottom: '8px',
        lineHeight: '1.18',
        paddingRight: '10px',
        wordBreak: 'break-word',
    },
    price: {
        color: '#333333',
        fontSize: '46px',
    },
    divContainer: {
        textAlign: 'initial'
    },
    titleDescription: {
        fontSize: '28px',
        fontWeight: '400',
        marginBottom: '32px',
    },
    description: {
        color: '#666666',
        fontSize: '16px',
        fontWeight: '400',
        marginBottom: '32px',
        whiteSpace: 'pre-line',
    },
    divDescription: {
        textAlign: 'justify',
        marginLeft: '32px',
    },
    grid: {
        alignSelf: 'flex-start',
    },
    typography: {
        color: '#999999',
        fontWeight: '600',
        fontSize: '14px',
    },
    spinner: {
        marginTop: '15px',
    }
});

export default styles;