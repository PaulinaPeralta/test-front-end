# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you should run the next three commands:


In a console run:

### `npm install`

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.


In another console run:

### `node server.js`
Runs the API at localhost:5000.
Open (http://localhost:5000/api) to view it in the browser.