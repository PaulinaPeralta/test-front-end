import React, { Component } from 'react';
import styles from './productDetailStyles';
import { withStyles } from '@material-ui/core/styles';
import { Card, CardContent, Grid, Button, CircularProgress } from '@material-ui/core';
import Breadcrumb from '../breadcrumb/breadcrumb.js';

class ProductDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            detail: {}
        }
    } 


    async componentDidMount() {
        const response = await fetch(`/api/items/${this.props.id}`);
        this.setState({
            detail: await response.json()               
        })
    }

      render() {
          const { classes, categories, formatNumber } = this.props;
          const { item } = this.state.detail;
          return (
              <div>
                  {item
                      ? <div>
                          <Breadcrumb categories={categories}></Breadcrumb>
                          <Card className={classes.root}>
                              <CardContent>
                                  <Grid
                                      container
                                      direction="row"
                                      justify="space-around"
                                      alignItems="center"
                                      spacing={2}>
                                      <Grid item xs={7}>
                                          <div className={classes.div}>
                                              <img className={classes.image} src={item.picture}></img>
                                          </div>
                                          <div className={classes.divDescription}>
                                              <div>
                                                  <span className={classes.titleDescription}>Descripción del producto</span>
                                              </div>
                                              <div>
                                                  <p className={classes.description}>{item.description}</p>
                                              </div>
                                          </div>
                                      </Grid>
                                      <Grid
                                          container
                                          item xs={5}
                                          direction="column"
                                          justify="flex-start"
                                          alignItems="flex-start"
                                          className={classes.grid}>
                                          <div className={classes.divContainer}>
                                              <div className={classes.divSpan1}>
                                                  <span className={classes.span1}>{item.condition === 'new' && 'Nuevo'} - {item.sold_quantity} vendidos</span>
                                              </div>
                                              <div className={classes.divSpan2}>
                                                  <span className={classes.span2}>{item.title}</span>
                                              </div>
                                              <div>
                                                  <span className={classes.price}>${formatNumber(item.price)}</span>
                                              </div>
                                              <Button className={classes.button}>
                                                  <span className={classes.span}>Comprar</span>
                                              </Button>
                                          </div>
                                      </Grid>
                                  </Grid>
                              </CardContent>
                          </Card>
                      </div>
                      : <CircularProgress className={classes.spinner}/>
                  }
              </div>
          );
    }
}

export default withStyles(styles)(ProductDetail);